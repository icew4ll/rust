use thiserror::Error;
use anyhow::Result;

#[derive(Debug, Error)]
#[error("ErrorA occurred")]
struct ErrorA {
    #[from]
    source: ErrorB
}

#[derive(Debug, Error)]
#[error("ErrorB occurred")]
struct ErrorB;

async fn returns_error_a() -> Result<bool,ErrorA> {
    Err(ErrorA {source: ErrorB})
}

async fn returns_error_b() -> Result<bool,ErrorB> {
    Err(ErrorB)
}

#[async_std::main]
async fn main() -> Result<()> {
    returns_error_a().await?;
    returns_error_b().await?;
    Ok(())
}
