use async_std::fs::File;
use async_std::io;
use async_std::prelude::*;
use async_std::task;

fn main() {
    task::block_on(read_file("../../Cargo.toml"));
}
async fn read_file(path: &str) -> io::Result<String> {
    let mut file = File::open(path).await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    println!("{}", buffer);
    Ok(buffer)
}
