use async_std::task;
// use futures::join;
// use futures::future::try_join_all;
use futures::future::join_all;

fn main() {
    let urls = vec![
        "https://httpbin.org/get",
        "https://rust-lang.github.io/async-book/01_getting_started/02_why_async.html",
        "https://blog.logrocket.com/a-practical-guide-to-async-in-rust/",
    ];
    join(urls);
}

// fn join(urls: Vec<&str>) -> Vec<std::result::Result<(&str, Vec<u8>), surf::Error>> {
//     task::block_on(async {
//         let results = urls.into_iter().map(|url| async move {
//             let mut res = surf::get(url).await?;
//             let bytes = res.body_bytes().await?;
//             Ok::<_, _>((url, bytes))
//         });

//         let results: Vec<std::result::Result<(&str, Vec<u8>), _>> = join_all(results).await;
//         dbg!(&results);
//         results
//     })
// }

fn join(urls: Vec<&str>) -> Vec<Result<(&str, String), surf::Error>> {
    task::block_on(async {
        let results = urls.into_iter().map(|url| async move {
            let mut res = surf::get(url).await?;
            let bytes = res.body_string().await?;
            Ok((url, bytes))
        });
        let results = join_all(results).await;
        dbg!(&results);
        results
    })
}

// async fn get_url(url: &str) -> surf::Result<String, String> {
//     let mut res = surf::get(url).await?;
//     // dbg!(res.body_bytes().await?);
//     let future = res.body_string().await?;
//     Ok(future)
// }
