use anyhow::Result;
use async_process::{ Command, Output };

#[async_std::main]
async fn main() -> Result<()> {
    println!("Restart\n");
    rust().await;
    Ok(())
}

async fn rust() -> Result<()> {
    let cmd = Command::new("sudo")
        .arg("ls")
        .output()
        .await?;
    dbg!(cmd);
    // Ok(())
    Ok(())
}
