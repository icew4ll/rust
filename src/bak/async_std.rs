use anyhow::Result;
use thiserror::Error;

async fn gets(url: &str) -> Result<String> {
    // dbg!(surf::get("https://httpbin.org/get").recv_string().await?);
    let mut res = surf::get(url).await?;
    Ok(res.body_string().await?)
}

#[async_std::main]
async fn main() -> Result<()> {
    let url = "https://github.com/trending";
    gets(url).await?;
    Ok(())
}
