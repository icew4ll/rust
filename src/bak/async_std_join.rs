use futures::future::join_all;
use futures::FutureExt as BoxedExt;
use futures::join;

async fn a() -> u8 {
    println!("a");
    async_std::task::sleep(std::time::Duration::from_secs(3)).await;
    println!("waited");
    1u8
}

async fn b() -> u8 {
    println!("b");
    async_std::task::sleep(std::time::Duration::from_secs(3)).await;
    println!("waited");
    2u8
}

#[async_std::main]
async fn main() {
    println!("syncronous\n");
    a().await;
    b().await;

    println!("asyncronous\n");
    assert_eq!(join!(a(), b()), (1, 2));

    println!("join_all\n");
    let futures: Vec<_> = vec![a().boxed(), b().boxed(), a().boxed()];
    assert_eq!(join_all(futures).await, [1, 2, 1]);

    // println!("raced\n");
    // let result = a().race(b()).await;
    // println!("raced {:?}\n", result);
}

// use async_std::fs::File;
// use async_std::io;
// use async_std::prelude::*;
// use async_std::task;
//
// fn main() {
//     task::block_on(read_file("../../Cargo.toml"));
// }
// async fn read_file(path: &str) -> io::Result<String> {
//     let mut file = File::open(path).await?;
//     let mut buffer = String::new();
//     file.read_to_string(&mut buffer).await?;
//     println!("{}", buffer);
//     Ok(buffer)
// }

// use std::{fs::File, io::prelude::*};
// use yaml_rust::{YamlLoader, yaml::{Hash, Yaml}};
//
// fn main() {
//     let file = "../data/config.yml";
//     dbg!(load_file(file));
// }
//
// fn load_file(file: &str) {
//     let mut file = File::open(file).expect("Unable to open file");
//     let mut contents = String::new();
//     file.read_to_string(&mut contents)
//         .expect("Unable to read file");
//     YamlLoader::load_from_str(&contents).unwrap();
// }
