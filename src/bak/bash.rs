// serde json {{{
#[macro_use]
extern crate serde_derive;

use serde::{Deserialize, Serialize};
use serde_json::Error;
use std::fs::File;
use std::io::BufReader;

use std::fs;
// }}}
// tokio {{{
// tokio process
use std::process::Command;
use std::str;
use tokio::prelude::*;
use tokio_process::CommandExt;
// }}}
macro_rules! vec_of_strings {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}

#[derive(Serialize, Deserialize, Debug)]
struct Server {
    name: String,
    ip: String,
    user: String,
    pass: String,
}

impl Server {
    fn csv(path: &str) -> Result<Vec<Self>, Error> {
        let json_file = File::open(path).expect("file not found");
        let reader = BufReader::new(json_file);
        let array: Vec<Server> = serde_json::from_reader(reader)?;
        dbg!(&array);
        Ok(array)
    }
    fn streamer(servers: Vec<Self>) -> Result<(), Error> {
        let mut cmd = vec_of_strings![];
        for server in servers {
            let login = vec_of_strings![format!("echo {}", server.ip)];
            cmd.push([&login[..]].concat().join(";"));
        }
        tokio::run(stream::iter_ok(cmd).for_each(|iter| {
            let future = Command::new("bash")
                .arg("-c")
                .arg(&iter)
                .output_async()
                .map_err(|e| println!("failed to collect output: {}", e))
                .map(|output| {
                    dbg!(output);
                });
            tokio::spawn(future);
            future::ok(())
        }));
        Ok(())
    }
}

fn main() {
    match Server::csv("test.json") {
        Ok(csv) => {
            println!("{:?}", csv);
            match Server::streamer(csv) {
                Ok(()) => {
                    println!("ok");
                }
                Err(e) => {
                    eprintln!("STREAMER ERROR: {}", e);
                }
            }
            //let serialize = serde_json::to_string(&csv[0]).unwrap();
            //println!("{}", serialize);
            //fs::write("./t3.json", serialize).expect("Unable to write file");
        }
        Err(e) => {
            eprintln!("CSV ERROR: {}", e);
        }
    }
}
//mailq | grep -i $(date +%^b) | grep "noreply@deskatmail.com" | awk '{print $1}' | postsuper -d -
//let sshopt =
//String::from("-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no");

//let login = vec_of_strings![
//"set prompt {[#|%|>|$] $}\n",
//format!("spawn ssh {} {}@{}", sshopt, server.user, server.ip),
//"expect \"assword\"",
//format!("send \"{}\n\"", server.pass),
//"expect $prompt",
//"send \"whoami\n\"",
//"expect $prompt"
//];

////
//let exit = vec_of_strings!["send \"exit\n\"", "expect $prompt"];

//cmd.push([&login[..], &exit[..]].concat().join(";"));
