use async_std::fs::File;
use async_std::prelude::*;
use yaml_rust::YamlLoader;
use anyhow::Result;
use async_process::{ Command};

async fn proc() -> Result<()> {
    let cmd = Command::new("sudo")
        .arg("ls")
        .output()
        .await?;
    dbg!(cmd);
    Ok(())
}

async fn read_file(path: &str) -> Result<()> {
    let mut file = File::open(path).await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    let docs = YamlLoader::load_from_str(&buffer).unwrap();
    dbg!(&docs[0]);
    let arr = &docs[0]["build"]["nnn"]["cmds"];
    dbg!(arr);
    dbg!(docs);
    Ok(())
}

async fn cli() -> Result<()> {
    use clap::{load_yaml, App};
    let yaml = load_yaml!("args.yml");
    let m = App::from(yaml).get_matches();

    if let Some(mode) = m.value_of("mode") {
        match mode {
            "vi" => proc().await?,
            _ => unreachable!(),
        }
    } else {
        println!("--mode <MODE> wasn't used...");
    }
    Ok(())
}

#[async_std::main]
async fn main() -> Result<()> {
    cli().await?;
    read_file("../data/config.yml").await?;
    Ok(())
}
