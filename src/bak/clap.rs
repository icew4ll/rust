#![allow(dead_code)]
use rexpect::errors::*;
use rexpect::spawn;
use std::io::{BufRead, BufReader};
use std::process::Command;
use subprocess::{Exec, Redirection};

fn t4() {
    // let t = Exec::shell("sudo ls").stream_stdout()?;
    let t = Exec::shell("sudo ls").stdout(Redirection::Pipe).capture().unwrap().stdout_str();
    println!("{:?}", t)
}

fn t3() -> Result<()> {
    let mut p = spawn("", Some(30_000))?;
    p.exp_regex("Name \\(.*\\):")?;
    p.send_line("anonymous")?;
    p.exp_string("Password")?;
    p.send_line("test")?;
    p.exp_string("ftp>")?;
    p.send_line("cd upload")?;
    p.exp_string("successfully changed.\r\nftp>")?;
    p.send_line("pwd")?;
    p.exp_regex("[0-9]+ \"/upload\"")?;
    p.send_line("exit")?;
    p.exp_eof()?;
    Ok(())
}

fn test() {
    let x = Exec::cmd("ls").stream_stdout().unwrap();
    let br = BufReader::new(x);
    for (i, line) in br.lines().enumerate() {
        println!("{}: {}", i, line.unwrap());
    }
}

fn t2() {
    Command::new("sudo")
        .arg("ls")
        .spawn()
        .expect("ls command failed to start");
}

fn main() {
    use clap::{load_yaml, App};

    let yaml = load_yaml!("args.yml");
    let m = App::from(yaml).get_matches();

    if let Some(mode) = m.value_of("mode") {
        match mode {
            "vi" => t4(),
            _ => unreachable!(),
        }
    } else {
        println!("--mode <MODE> wasn't used...");
    }
}
