use async_process::{Command, Output};
// use async_std::process::Command;
use async_std::task;
use structopt::StructOpt;

fn main() {
    let opt = Opt::from_args();
    task::block_on(rust());
    // match opt.sub {
    //     Sub::Rust => task::block_on(task::spawn(rust())),
    //     Sub::Lua => task::block_on(task::spawn(rust())),
    // }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "ice")]
struct Opt {
    #[structopt(subcommand)]
    sub: Sub,
}

#[derive(Debug, StructOpt)]
enum Sub {
    Lua,
    Rust,
}

async fn rust() -> Result<async_process::Output, async_std::io::Error> {
    url = "https://github.com/sumneko/lua-language-server";
    let results = std::str::from_utf8(git(url).stdout).unwrap();
    println!(results);
    Ok(())
}

async fn git(url: &str) -> Result<async_process::Output, async_std::io::Error> {
    let cmd = Command::new("git")
        .arg("clone")
        .arg(url)
        .current_dir("/tmp")
        .output()
        .await?;
    dbg!(&cmd);
    println!("{}", std::str::from_utf8(&cmd.stdout).unwrap());
    // Ok(())
    Ok(cmd)
}
