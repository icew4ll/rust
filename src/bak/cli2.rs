use anyhow::Result;
use async_std::fs::read_to_string;
use std::str;
use structopt::StructOpt;

const FILE: &str = "Cargo.toml";

async fn test() -> Result<String> {
    let contents = read_to_string(FILE).await?;
    Ok(contents)
}

#[async_std::main]
async fn main() -> Result<()> {
    match Opt::from_args().sub {
        Sub::Lua => dbg!(test().await?),
        Sub::Rust => dbg!(test().await?),
    };
    Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(name = "ice")]
struct Opt {
    #[structopt(subcommand)]
    sub: Sub,
}

#[derive(Debug, StructOpt)]
enum Sub {
    Lua,
    Rust,
}
