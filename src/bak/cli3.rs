//! How to extract subcommands' args into external structs.

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Foo {
    bar: Option<String>,
}

#[derive(Debug, StructOpt)]
enum Command {
    #[structopt(name = "foo")]
    Foo(Foo),
}

#[derive(Debug, StructOpt)]
#[structopt(name = "classify")]
struct ApplicationArguments {
    #[structopt(subcommand)]
    command: Command,
}

fn main() {
    let opt = ApplicationArguments::from_args();
    println!("{:?}", opt);
}
