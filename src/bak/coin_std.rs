#[macro_use]
extern crate fstrings;
use async_std::task;
use structopt::StructOpt;

fn main() {
    let opt = Opt::from_args();
    println_f!("{:?}", opt.subcommand);
    match opt.subcommand {
        Subcommand::Test => t1(),
    };
}

#[derive(StructOpt, Debug)]
#[structopt(name = "coin")]
struct Opt {
    #[structopt(subcommand)]
    subcommand: Subcommand,
}

#[derive(Debug, StructOpt)]
enum Subcommand {
    Test,
}

fn t1() {
    let task = task::spawn(async {
        let result = t5().await;
        match result {
            Ok(s) => println!("{:?}", s),
            Err(e) => println!("Error reading file: {:?}", e),
        }
    });
    println!("Started task!");
    task::block_on(task);
    println!("Stopped task!");
}

fn t2() {
    println!("Hello, world!");
}

fn t3() -> Result<(), surf::Error> {
    task::block_on(async {
        let uri = "https://api.coingecko.com/api/v3/coins";
        let res = surf::get(uri).recv_json().await?;
        println!("{:?}", &res);
        Ok(res)
    })
}

async fn t4() -> surf::Result<()> {
    dbg!(surf::get("https://httpbin.org/get").recv_string().await?);
    Ok(())
}

async fn t5() -> surf::Result<()> {
    // let uri = "https://api.coingecko.com/api/v3/coins";
    // println!("{:?}", surf::get(uri).recv_json().await?);
    let uri = "https://api.ipify.org?format=json";
    let res = surf::get(uri).recv_json().await?;
    println!("{:?}", res);
    Ok(())
}
