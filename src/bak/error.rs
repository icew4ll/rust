use anyhow::{Result};
use async_std::fs::read_to_string;
use std::str;

const FILE: &str = "Cargo.toml";

async fn test() -> Result<String> {
    let contents = read_to_string(FILE).await?;
    Ok(contents)
}

#[async_std::main]
async fn main() -> Result<()> {
    dbg!(test().await?);
    Ok(())
}
