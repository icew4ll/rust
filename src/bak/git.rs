use anyhow::Result;
use git2::Error;
use git2::Repository;
use git2::Status;

async fn a() -> Result<()> {
    let repo = match Repository::open("/home/ice/m/rust") {
        Ok(repo) => repo,
        Err(e) => panic!("failed to init: {}", e),
    };
    let status = match repo.status_file(std::path::Path::new("Cargo.toml")) {
        Ok(status) => dbg!(status),
        Err(e) => panic!("failed to init: {}", e),
    };
    Ok(())
}

#[async_std::main]
async fn main() -> Result<()> {
    println!("Restart\n");
    a().await;
    Ok(())
}
