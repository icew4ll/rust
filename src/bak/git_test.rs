use anyhow::Result;
use git2::Error;
use git2::Repository;
use git2::Status;

async fn a() -> Result<Status, Error> {
    let repo = Repository::open("/home/ice/m/rust")?;
    // dbg!(repo.status_file(std::path::Path::new("./Cargo.toml")));
    Ok(repo.status_file(std::path::Path::new("Cargo.toml"))?)
}

#[async_std::main]
async fn main() -> Result<()> {
    println!("Restart\n");
    dbg!(a().await); 
    Ok(())
}
