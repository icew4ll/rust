#[macro_use]
extern crate fstrings;
use std::collections::HashMap;
use std::fs::read_to_string;

fn main() {
    let mut arguments = std::env::args().skip(1);
    // key is a string not null, null values handled by unwrap and result in panic
    let key = arguments.next().unwrap();
    let value = arguments.next().unwrap();
    println_f!("key: {key} value: {value}");

    // let contents = f!("{key} {value}");
    // println_f!("{contents}");
    // std::fs::write("kv.db", contents).unwrap();

    let mut database = Database::new().expect("db create failed");
    database.insert(key.to_uppercase(), value.clone());
    database.insert(key, value);
    database.flush().unwrap();
}

struct Database {
    map: HashMap<String, String>,
}

impl Database {
    fn new() -> Result<Database, std::io::Error> {
        let mut map = HashMap::new();
        // contents is owned string, dropped at end of scope unless transferred
        let contents = read_to_string("kv.db")?;
        for line in contents.lines() {
            let mut chunks = line.splitn(2, '\t');
            let key = chunks.next().expect("no key");
            let value = chunks.next().expect("no value");
            map.insert(key.to_owned(), value.to_owned());
        }
        Ok(Database { map })
    }

    fn insert(&mut self, key: String, value: String) {
        self.map.insert(key, value);
    }

    // taking ownership can be used for api design
    // use case: flush should be the last use of database
    // can be modeled with self taking ownership
    fn flush(self) -> std::io::Result<()> {
        do_flush(&self)
    }
}

impl Drop for Database {
    fn drop(&mut self) {
        let _ = do_flush(self);
    }
}

fn do_flush(database: &Database) -> std::io::Result<()> {
    let mut contents = String::new();
    for (key, value) in &database.map {
        contents.push_str(key);
        contents.push('\t');
        contents.push_str(value);
        contents.push('\n');
    }
    std::fs::write("kv.db", contents)
}
