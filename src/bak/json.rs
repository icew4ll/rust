#[macro_use]
extern crate serde_derive;

use serde_json::Error;
use std::fs::File;
use std::io::BufReader;

#[derive(Serialize, Deserialize, Debug)]
struct Json {
    name: String,
    ip: String,
    user: String,
    pass: String,
}
impl Json {
    fn read(path: &str) -> Result<(), Error> {
        let json_file = File::open(path).expect("file not found");
        let reader = BufReader::new(json_file);
        let array: Vec<Json> = serde_json::from_reader(reader)?;
        //let p: Json = serde_json::from_reader(reader)?;
        dbg!(&array);
        Ok(())
    }
}

fn main() {
    match Json::read("test.json") {
        Ok(i) => println!("{:?}", i),
        Err(e) => {
            eprintln!("An error occurred: {}", e);
        }
    }
}
