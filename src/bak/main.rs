#[macro_use]
extern crate dotenv_codegen;

#[macro_use]
extern crate serde_derive;

use serde::{Deserialize, Serialize};
//use serde_json::Error;
//use failure::Error;
use std::fs::File;
use std::io::BufReader;

// write
use std::fs;

// dotenv
use dotenv::dotenv;
use std::env;

use std::sync::mpsc;

use std::process::Command;
use std::str;
use tokio::prelude::*;
use tokio_process::CommandExt;

use chrono::prelude::*;
use colored::*;
use structopt::StructOpt;

macro_rules! vec_of_strings {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}

#[derive(Serialize, Deserialize, Debug)]
struct Json {
    config: Vec<Config>,
    projects: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    name: String,
    dir: String,
}

#[derive(StructOpt, Debug)]
#[structopt(name = "adam")]
enum Opt {
    #[structopt(name = "push")]
    /// push repo
    Push,
    #[structopt(name = "pull")]
    /// pull repo
    Pull,
}

impl Json {
    fn file(path: &str) -> Result<Self, serde_json::Error> {
        let json_file = File::open(path).expect("file not found");
        let reader = BufReader::new(json_file);
        let json: Json = serde_json::from_reader(reader)?;
        dbg!(&json);
        Ok(json)
    }
    fn streamer(json: Self) -> Result<(), failure::Error> {
        // get home directory
        dotenv().ok();
        let home = env::var("HOME").ok().unwrap();

        // commands to push to git
        fn push(json: Json, home: String) -> Result<Vec<String>, failure::Error> {
            // command container
            let mut cmd = vec_of_strings![];
            // backup configs to dot
            for config in json.config {
                let login = vec_of_strings![
                    format!(
                        "echo rsync -av {}/{}{} {}/m/dot",
                        &home, config.dir, config.name, &home
                    ),
                    format!(
                        "rsync -av {}/{}{} {}/m/dot",
                        &home, config.dir, config.name, &home
                    )
                ];
                cmd.push([&login[..]].concat().join(" ; "));
            }

            // git push projects
            for project in json.projects {
                let push = vec_of_strings![
                    format!("echo cd {}/{}", home, project),
                    format!("cd {}/{}", home, project),
                    "git add .",
                    "git commit -m 'test'",
                    "git push"
                ];
                cmd.push([&push[..]].concat().join(" ; "));
            }
            dbg!(&cmd);
            Ok(cmd)
        }

        // commands to pull from git
        fn pull(json: Json, home: String) -> Result<Vec<String>, failure::Error> {
            // command container
            let mut cmd = vec_of_strings![];
            // git push projects
            for project in json.projects {
                let push = vec_of_strings![
                    format!("echo cd {}/{}", home, project),
                    format!("cd {}/{}", home, project),
                    "git pull"
                ];
                cmd.push([&push[..]].concat().join(" ; "));
            }

            // backup configs to dot
            for config in json.config {
                let login = vec_of_strings![
                    format!(
                        "echo rsync -av {}/m/dot/{} {}/{}",
                        &home, config.name, &home, config.dir
                    ),
                    format!(
                        "rsync -av {}/m/dot/{} {}/{}",
                        &home, config.name, &home, config.dir
                    )
                ];
                cmd.push([&login[..]].concat().join(" ; "));
            }
            dbg!(&cmd);
            Ok(cmd)
        }

        // match cli args and build command
        let cmd = match Opt::from_args() {
            Opt::Push => push(json, home).unwrap(),
            Opt::Pull => pull(json, home).unwrap(),
        };

        // process executor
        tokio::run(stream::iter_ok(cmd).for_each(|iter| {
            let future = Command::new("bash")
                .arg("-c")
                .arg(&iter)
                .output_async()
                .map_err(|e| println!("failed to collect output: {}", e))
                .map(|output| {
                    fn pack(output: std::process::Output) -> Result<(), failure::Error> {
                        let stdout = str::from_utf8(&output.stdout)?;
                        println!("{}", stdout);
                        Ok(())
                    }
                    pack(output).unwrap();
                });
            tokio::spawn(future);
            future::ok(())
        }));
        Ok(())
    }
}
fn main() {
    match Json::file("config.json") {
        Ok(file) => match Json::streamer(file) {
            Ok(()) => {}
            Err(e) => {
                eprintln!("STREAMER ERROR: {}", e);
            }
        },
        Err(e) => {
            eprintln!("File ERROR: {}", e);
        }
    }
}
