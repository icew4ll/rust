// imports {{{
// regex
#[macro_use]
extern crate lazy_static;
use regex::Regex;
// tokio process
use std::process::Command;
use std::str;
use tokio::prelude::*;
use tokio_process::CommandExt;

// structopt
use structopt::StructOpt;

// errors
use failure::Error;
use std::process;

use serde::{Deserialize, Serialize};
use std::fs::File;
//use serde_json::Result;
use serde_json::{Result, Value};
//use failure::Error;
use std::io::BufReader;
use std::path::Path;

// }}}
// pack {{{
#[derive(Serialize, Deserialize, Debug)]
struct Json {
    name: String,
    ip: String,
    user: String,
    pass: String,
}
impl Json {
    fn read(path: &str) -> Result<Self> {
        let json_file = File::open(path).expect("file not found");
        let reader = BufReader::new(json_file);
        let p: Json = serde_json::from_reader(reader)?;
        dbg!(&p);
        Ok(p)
    }
}
// }}}
// main {{{
fn main() {
    match Json::read("test.json") {
        Ok(i) => println!("{}", i.name),
        Err(e) => {
            eprintln!("An error occurred: {}", e);
        }
    }
}
// }}}
