// imports {{{
#[macro_use]
extern crate dotenv_codegen;

#[macro_use]
extern crate serde_derive;

use serde::{Deserialize, Serialize};
//use serde_json::Error;
//use failure::Error;
use std::fs::File;
use std::io::BufReader;
// write
use std::fs;

// dotenv
use dotenv::dotenv;
use std::env;

// message sending
use std::sync::mpsc;
// }}}
// tokio {{{
// tokio process
use std::process::Command;
use std::str;
use tokio::prelude::*;
use tokio_process::CommandExt;
// }}}
// macros {{{
macro_rules! vec_of_strings {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}
// }}}
// struct {{{
#[derive(Serialize, Deserialize, Debug)]
struct Server {
    name: String,
    ip: String,
    user: String,
    pass: String,
}

impl Server {
    fn csv(path: &str) -> Result<Vec<Self>, serde_json::Error> {
        let json_file = File::open(path).expect("file not found");
        let reader = BufReader::new(json_file);
        let array: Vec<Server> = serde_json::from_reader(reader)?;
        dbg!(&array);
        Ok(array)
    }
    fn streamer(servers: Vec<Self>) -> Result<(), failure::Error> {
        let mut cmd = vec_of_strings![];
        for server in servers {
            let sshopt =
                String::from("-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no");

            let login = vec_of_strings![
                "set prompt {[#|%|>|$] $}\n",
                format!(
                    "spawn ssh {} {}@{}",
                    sshopt,
                    env::var(server.user)?,
                    server.ip
                ),
                "expect \"assword\"",
                format!("send \"{}\n\"", env::var(server.pass)?),
                "expect $prompt",
                "send \"whoami\n\"",
                "expect $prompt"
            ];

            //
            let exit = vec_of_strings!["send \"exit\n\"", "expect $prompt"];

            cmd.push([&login[..], &exit[..]].concat().join(";"));
        }
        tokio::run(stream::iter_ok(cmd).for_each(|iter| {
            let future = Command::new("expect")
                .arg("-c")
                .arg(&iter)
                .output_async()
                .map_err(|e| println!("failed to collect output: {}", e))
                .map(|output| {
                    //dbg!(output);
                    fn pack(output: std::process::Output) -> Result<(), failure::Error> {
                        let stdout = str::from_utf8(&output.stdout)?;
                        let split = stdout.split("\r\n").collect::<Vec<_>>();
                        dbg!(split.iter().filter(|&i| i.contains("sscom")));
                        //let sscom = split
                        //.iter()
                        //.filter(|&i| i.contains("sscom"))
                        //.map(|i| i.replace("sscom", ""))
                        //.collect::<Vec<_>>();
                        Ok(())
                    }
                    pack(output).unwrap();
                });
            tokio::spawn(future);
            future::ok(())
        }));
        Ok(())
    }
}
// }}}

fn main() {
    dotenv().ok();
    match Server::csv("test.json") {
        Ok(csv) => {
            println!("{:?}", csv);
            match Server::streamer(csv) {
                Ok(()) => {}
                Err(e) => {
                    eprintln!("STREAMER ERROR: {}", e);
                }
            }
            //let serialize = serde_json::to_string(&csv[0]).unwrap();
            //println!("{}", serialize);
            //fs::write("./t3.json", serialize).expect("Unable to write file");
        }
        Err(e) => {
            eprintln!("CSV ERROR: {}", e);
        }
    }
}
//mailq | grep -i $(date +%^b) | grep "noreply@deskatmail.com" | awk '{print $1}' | postsuper -d -
//let sshopt =
//String::from("-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no");

//let login = vec_of_strings![
//"set prompt {[#|%|>|$] $}\n",
//format!("spawn ssh {} {}@{}", sshopt, server.user, server.ip),
//"expect \"assword\"",
//format!("send \"{}\n\"", server.pass),
//"expect $prompt",
//"send \"whoami\n\"",
//"expect $prompt"
//];

////
//let exit = vec_of_strings!["send \"exit\n\"", "expect $prompt"];

//cmd.push([&login[..], &exit[..]].concat().join(";"));
