use structopt::StructOpt;

fn main() {
    let opt = Opt::from_args();
    match opt.sub {
        Sub::Lua => println!("lua"),
        Sub::Rust => println!("rust"),
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "ice")]
struct Opt {
    #[structopt(subcommand)]
    sub: Sub,
}

#[derive(Debug, StructOpt)]
enum Sub {
    Lua,
    Rust,
}
