#[macro_use]
extern crate fstrings;
use structopt::StructOpt;

fn main() {
    let opt = Opt::from_args();
    // println_f!("{opt.subcommand}")
    // println_f!("{:?}", opt.subcommand);
    match opt.subcommand {
        Subcommand::Install(i) => do_install(),
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "ice")]
struct Opt {
    #[structopt(subcommand)]
    pub subcommand: Subcommand,
}

#[derive(Debug, StructOpt)]
enum Subcommand {
    Install(Install),
}

#[derive(Debug, StructOpt)]
pub struct Install {}

fn do_install() {
    let test = "rust";
    println_f!("Rewrite in {test}!");
}
