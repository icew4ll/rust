use serde_json::Value;
use async_std::task;
use surf;

fn main() {
    let url = "https://blockchain.info/ticker";
    println!("Send request to: {}", url);

    task::block_on(async {
        // first way, auto parse returned string as json
        let val: Value = surf::get(url).recv_json().await.unwrap();
        println!("Bitcoin Price: ${}", val["USD"]["last"]);

        // second way, use returned string
        let valstr = surf::get(url).recv_string().await.unwrap();
        let val: Value = serde_json::from_str(&valstr).unwrap();
        println!("Bitcoin Price: ${}", val["USD"]["last"]);
    });
}
