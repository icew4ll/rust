// imports {{{
// regex
#[macro_use]
extern crate lazy_static;
use regex::Regex;
// tokio process
use std::process::Command;
use std::str;
use tokio::prelude::*;
use tokio_process::CommandExt;

// structopt
use structopt::StructOpt;

// errors
use failure::Error;
use std::process;

// csv
#[macro_use]
extern crate serde_derive;
use std::fs;
use std::path::Path;
// }}}
// structs {{{
#[derive(StructOpt, Debug)]
#[structopt(name = "git")]
enum Opt {
    #[structopt(name = "jb")]
    /// restart keepalived service
    Jb { arg0: Vec<String> },
}

// cmds
#[derive(Debug, Deserialize)]
struct Cmds {
    cmd: Vec<String>,
}

macro_rules! vec_of_strings {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}

// regex
lazy_static! {
    pub static ref ISIP: Regex = Regex::new(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$").unwrap();
}

// }}}
// Cmds {{{
impl Cmds {
    fn makecmds(arg0: Vec<String>) -> impl Stream<Item = String, Error = ()> {
        let mut cmd = vec_of_strings![];
        for i in arg0 {
            if ISIP.is_match(&i) == true {
                let sshopt =
                    String::from("-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no");

                let login = vec_of_strings![
                    "set prompt {[#|%|>|$] $}\n",
                    format!("spawn ssh {} {}@{}", sshopt, "root", i),
                    "expect \"assword\"",
                    format!("send \"{}\n\"", "g3gAWES#4%"),
                    "expect $prompt",
                    "send \"mailq | grep -i \\$(date +%^b) | grep 'noreply@deskatmail.com' | awk '{print \\$1}' | postsuper -d -\n\"",
                    "expect $prompt"
                ];

                //mailq | grep -i $(date +%^b) | grep "noreply@deskatmail.com" | awk '{print $1}' | postsuper -d -
                //
                let exit = vec_of_strings!["send \"exit\n\"", "expect $prompt"];

                cmd.push([&login[..], &exit[..]].concat().join(";"));
            } else {
                println!("ip check failed");
            }
        }
        stream::iter_ok(cmd)
    }
}
// }}}
// pack {{{
fn pack(output: std::process::Output) -> Result<(), Error> {
    let stdout = str::from_utf8(&output.stdout)?;
    println!("{}", stdout);
    Ok(())
}
// }}}
// main {{{
fn main() {
    let subcommand = Opt::from_args();
    let cmds = match subcommand {
        Opt::Jb { arg0 } => Cmds::makecmds(arg0),
    };

    tokio::run(cmds.for_each(|iter| {
        let future = Command::new("expect")
            .arg("-c")
            .arg(&iter)
            .output_async()
            .map_err(|e| println!("failed to collect output: {}", e))
            .map(|output| {
                //dbg!(output);
                //println!("{:?}", str::from_utf8(&output.stdout));
                if let Err(err) = pack(output) {
                    println!("{}", err);
                    process::exit(1);
                }
            });
        tokio::spawn(future);
        future::ok(())
    }));
}
// }}}
// w0,216.230.241.172,UR,PG
// w1,216.230.241.174,UR,PG
// loop-rs --every 3m -- /home/fish/m/rust/target/release/rust jb 216.230.241.172 216.230.241.174
