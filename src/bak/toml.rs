use anyhow::Result;
use async_std::fs::read_to_string;

async fn test() -> Result<String> {
    let toml_str = r#"
            [ips]
            anyhow = "1.0.40"
            "#;
    let contents = toml::from_str(toml_str)?;
    Ok(contents)
}

#[async_std::main]
async fn main() -> Result<()> {
    dbg!(test().await?);
    Ok(())
}
