use async_std::fs::File;
use async_std::prelude::*;
use yaml_rust::YamlLoader;
use anyhow::Result;

async fn read_file(path: &str) -> Result<()> {
    let mut file = File::open(path).await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    let docs = YamlLoader::load_from_str(&buffer).unwrap();
    dbg!(docs);
    Ok(())
}

#[async_std::main]
async fn main() -> Result<()> {
    dbg!(read_file("../data/config.yml").await?);
    Ok(())
}
