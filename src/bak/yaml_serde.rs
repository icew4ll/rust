use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Point {
    x: f64,
    y: f64,
}

#[async_std::main]
async fn main() -> Result<()> {
    println!("RESET");
    simple().await?;
    derive().await?;
    Ok(())
}

async fn simple() -> Result<()> {
    // You have some type.
    let mut map = BTreeMap::new();
    map.insert("x".to_string(), 1.0);
    map.insert("y".to_string(), 2.0);

    // Serialize it to a YAML string.
    let s = serde_yaml::to_string(&map)?;
    assert_eq!(s, "---\nx: 1.0\ny: 2.0\n");

    // Deserialize it back to a Rust type.
    let deserialized_map: BTreeMap<String, f64> = serde_yaml::from_str(&s)?;
    dbg!(&deserialized_map);
    assert_eq!(map, deserialized_map);
    Ok(())
}

async fn derive() -> Result<()> {
    let point = Point { x: 1.0, y: 2.0 };

    let s = serde_yaml::to_string(&point)?;
    assert_eq!(s, "---\nx: 1.0\ny: 2.0\n");

    let deserialized_point: Point = serde_yaml::from_str(&s)?;
    dbg!(&deserialized_point);
    assert_eq!(point, deserialized_point);
    Ok(())
}
