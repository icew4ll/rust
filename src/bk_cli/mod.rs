use anyhow::Result;
use async_process::Command;
use async_std::fs::File;
use async_std::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Cfg {
    installs: Vec<Bin>,
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Bin {
    url: String,
    bin: String,
    cmds: Vec<String>,
}

pub async fn yaml() -> Result<()> {
    let mut file = File::open("/home/ice/m/rust/src/data/config.yml").await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    let deserialized: Cfg = serde_yaml::from_str(&buffer)?;
    // dbg!(&deserialized.installs);
    // let array = &deserialized.installs;
    let array_iter = deserialized.installs.into_iter();
    // dbg!(array_iter.filter(|x| { let _: () = x; x == "nnn" }));
    dbg!(array_iter.filter(|x| x.bin == "nnn").collect::<Vec<_>>());
    Ok(())
}

pub async fn installer() -> Result<()> {
    let cmd = Command::new("sudo").arg("ls").output().await?;
    dbg!(cmd);
    Ok(())
}

pub async fn args() -> Result<()> {
    use clap::{load_yaml, App};

    let yaml = load_yaml!("../data/args.yml");
    let m = App::from(yaml).get_matches();

    if let Some(repo) = m.value_of("repo") {
        println!("Selected {}", repo);
    } else {
        println!("--repo wasn't used...");
    }
    Ok(())
}

// pub async fn yml() -> Result<()> {
//     let point = Cfg { x: 1.0, y: 2.0 };
//
//     let s = serde_yaml::to_string(&point)?;
//     assert_eq!(s, "---\nx: 1.0\ny: 2.0\n");
//
//     let deserialized_point: Cfg = serde_yaml::from_str(&s)?;
//     dbg!(&deserialized_point);
//     assert_eq!(point, deserialized_point);
//     Ok(())
// }
