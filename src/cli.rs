use anyhow::Result;

pub async fn cli() -> Result<()> {
    use clap::{load_yaml, App};

    let yaml = load_yaml!("data/args.yml");
    let m = App::from(yaml).get_matches();

    if let Some(repo) = m.value_of("repo") {
        println!("Selected {}", repo);
    } else {
        println!("--repo wasn't used...");
    }
    Ok(())
}
