use anyhow::Result;
use async_process::Command;
use async_std::fs::File;
use async_std::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Cfg {
    installs: Vec<Bin>,
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Bin {
    url: String,
    bin: String,
    cmds: Vec<String>,
}

pub async fn yaml(repo: &str) -> Result<()> {
    let mut file = File::open("/home/ice/m/rust/src/data/config.yml").await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;
    let deserialized: Cfg = serde_yaml::from_str(&buffer)?;
    let array_iter = deserialized.installs.into_iter();
    let bin = &array_iter.filter(|x| x.bin == repo).collect::<Vec<_>>()[0];
    // dbg!(&bin);
    install(bin).await?;

    // command builder
    // let cmds = vec![]
    // let cmd = Command::new("").arg("ls").output().await?;
    Ok(())
}

async fn install(selected: &Bin) -> Result<()> {
    dbg!(selected);
    // build commands
    // let out = format!("{:?}/bin", dirs::home_dir().unwrap());
    // let url = "https://github.com/jarun/nnn";
    // let bin = "nnn";
    // let dest = format!("/tmp/{}", bin);
    // let cmds = &[
    //     format!("git clone {} {}", url, dest),
    //     format!("cd {}", dest),
    //     "make O_NERD=1 O_PCRE=1".to_string(),
    //     format!("mv nnn {}", out)
    // ].join(" && ");
    // check directory
    // if Path::new(&dest).exists().await {
    //     fs::remove_dir_all(&dest).await?;
    // }
    // dbg!(cmds);
    // run commands
    // let cmd = Command::new("ion")
    //     .arg("-c")
    //     .arg(cmds)
    //     .output()
    //     .await?;
    // dbg!(cmd);
    Ok(())
}
