use std::io::Read;

use anyhow::Result;
mod cli;
mod data;

async fn clap() -> Result<()> {
    use clap::{load_yaml, App};
    println!("RESET");

    let yaml = load_yaml!("data/args.yml");
    let m = App::from(yaml).get_matches();
    if let Some(repo) = m.value_of("repo") {
        println!("Selected {}", repo);
        dbg!(data::yaml(repo).await?);
    } else {
        println!("--repo wasn't used...");
    }
    Ok(())
}

fn main() {
    let input: Option<i32> = std::io::stdin()
        .bytes()
        .next()
        .and_then(|result| result.ok())
        .map(|byte| byte as i32);

    println!("{:?}", std::str::from_utf8(input.unwrap()));
}
