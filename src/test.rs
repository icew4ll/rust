use std::io::Read;

#[async_std::main]
async fn main() -> Result<()> {
    let input: Option<i32> = std::io::stdin()
        .bytes()
        .next()
        .and_then(|result| result.ok())
        .map(|byte| byte as i32);

    println!("{:?}", input);
    Ok(())
}
